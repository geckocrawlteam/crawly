const CsvStringifySync=require("csv-stringify/lib/sync");
const CsvParseSync=require("csv-parse/lib/sync");

class CsvUtil {
	static stringifyLine(data) {
		return CsvStringifySync([data],{
			eof: false
		});
	}

	static parseLine(line) {
		return CsvParseSync(line)[0];
	}
}

module.exports=CsvUtil;