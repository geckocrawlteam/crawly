let child_process=require("child_process");
let CrawlRunner=require("./CrawlRunner");
let WorkerObject=require("worker-object");

class ForkCrawlRunner extends CrawlRunner {

	constructor() {
		super();
	}

	async doRun() {
		this.worker=new WorkerObject(__dirname+"/ForkCrawlRunnerWorker.js");

		this.worker.on("close",async ()=>{
			this.error("Unexpected process termination");
		});

		this.worker.on("error",async (err)=>{
			await this.worker.terminate();
			this.error(err);
		});

		this.worker.on("progress",(percent)=>{
			this.progress(percent);
		});

		this.worker.on("log",(message)=>{
			this.log(message);
		});

		this.worker.on("result",(row)=>{
			this.result(row);
		});

		await this.worker.new(this.crawler.getAbsolutePath());
		await this.worker.call("setInputs",this.inputs);
		await this.worker.call("setPupOptions",this.pupOptions);
		await this.worker.call("setAuth",this.auth);

		this.worker.call("run")
			.then(async (data)=>{
				await this.worker.terminate();
				this.done();
			})
			.catch(async (err)=>{
				await this.worker.terminate();
				this.error(err);
			});
	}
}

module.exports=ForkCrawlRunner;