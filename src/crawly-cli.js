#!/usr/bin/env node

let Crawler=require("./Crawler.js");
let prompt=require("./prompt.js");
let argv=require('minimist')(process.argv.slice(2));
let fs=require("fs-extra");
let PupUtil=require("./PupUtil.js");
let crawler;
let Progress = require('progress');

function createCrawler(path) {
	let crawler;

	try {
		if (path===undefined)
			crawler=Crawler.createFromCwd();

		else
			crawler=Crawler.createFromPackage(path);
	}

	catch (e) {
		console.error(e);
		console.error(e.message);
		process.exit(1);
	}

	return crawler;
}

switch (process.argv[2]) {
	case "pupdl":
		PupUtil.ensureChromeInstalled().then(()=>{
			console.log(PupUtil.getStatusString());
		});
		break;

	case "pupstatus":
		console.log(PupUtil.getStatusString());
		break;

	case "info":
		crawler=createCrawler(argv._[1]);
		console.log("Name: "+crawler.getName());
		console.log("Path: "+crawler.getAbsolutePath());
		console.log("Inputs:");
		for (let input of crawler.getInputs()) {
			console.log("  "+input.name+" - "+input.label);
		}
		break;

	case "create":
		(async ()=> {
			if (argv._.length!=2) {
				console.log("No name given.");
				process.exit(1);
			}

			let name=argv._[1];
			let path=process.cwd()+"/"+name;

			if (fs.pathExistsSync(path)) {
				console.log(path+": Already exists.");
				process.exit(1);
			}

			fs.copySync(__dirname+"/../template",path);

			var package=fs.readJsonSync(path+"/package.json");
			package.name=name;
			package.crawler.name=name;
			fs.writeJsonSync(path+"/package.json",package,{spaces:"\t"});
			console.log(path+": Created crawler '"+name+"'.");
			console.log("Now try:");
			console.log();
			console.log("  cd "+path);
			console.log("  crawly info");
			console.log();
		})();
		break;

	case "run":
		crawler=createCrawler(argv._[1]);

		if (!PupUtil.isChromeInstalled()) {
			console.log("Crawly needs a local version of Chromium downloaded and installed.");
			console.log("It doesn't seem to be downloaded at the moment.");
			console.log();
			console.log("Install it using: crawly pupdl");
			process.exit(1);
		}

		(async ()=>{
			let inputs = crawler.getInputs();
			let userInput = {};

			for (let input of inputs) {
				if (argv[input.name])
					userInput[input.name]=argv[input.name];

				else if (input.default)
					userInput[input.name]=input.default;

				else
					userInput[input.name]=await prompt(
						"["+input.name+"] "+input.label+": "
					);
			}

			let outputFileName="crawly-output.json";
			if (argv.output)
				outputFileName=argv.output;

			let mode="base";
			if (argv.fork)
				mode="fork";

			let crawl=crawler.createCrawlRunner(mode);
			if (argv.headfull)
				crawl.setPupOption("headless",false);

			if (argv.proxy)
				crawl.setPupArg("--proxy-server="+argv.proxy);

			if (argv.auth)
				crawl.setAuth(argv.auth);

			crawl.setInputs(userInput);

			let lastProgress=0;
			let progressBar=new Progress(`Crawling: [:bar] :percent Rows: :results `, {
				complete: '=',
				incomplete: ' ',
				width: 20,
				total: 100,
				renderThrottle: 0
			});

			progressBar.tick(0,{
				results: 0
			});

			crawl.on("progress",(progress)=>{
				if (progress>=100)
					progress=99;

				progressBar.tick(progress-lastProgress);
				lastProgress=progress;
			});

			crawl.on("log",(message)=>{
				progressBar.interrupt("\r----> "+message);
			});

			crawl.on("result",(row)=>{
				progressBar.tick(0,{
					results: crawl.getData().length
				});
			});

			let startTime=Date.now();

			try {
				await crawl.run();
				progressBar.tick(100-lastProgress);
				let runTime=Date.now()-startTime;
				let data=crawl.getData();
				let perSec=data.length/(runTime/1000);

				console.log(
					"Done, rows: "+data.length+", "+
					"time: "+(runTime/1000)+"s, "+
					"rows/sec: "+(Math.round(perSec*1000)/1000));

				fs.writeJsonSync(outputFileName,data,{spaces:"\t"});
				console.log("Saved to: "+outputFileName);
			}

			catch (e) {
				progressBar.tick(100-lastProgress);

				console.log("Error running crawler: "+e);
				console.log(e);
			}
		})();
		break;

	default:
		console.log("Usage: crawly <command>");
		console.log("Commands: ");
		console.log("  info           - Prints info about the crawler in the current directory.");
		console.log("  run            - Runs the crawler in the current directory.");
		console.log("  create <name>  - Creates a crawler.");
		console.log("  pupdl          - Download puppeteer.")
		console.log("  pupstatus      - Check status of downloaded puppeteer.")
		break;
}