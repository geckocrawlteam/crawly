var path=require("path");
var fs=require("fs-extra");
var puppeteer=require("./puppeteer.js");

function toMegabytes(bytes) {
	const mb = bytes / 1024 / 1024;
	return `${Math.round(mb * 10) / 10} Mb`;
}

class PupUtil {

	static getStatusString() {
		let rev=PupUtil.getCanonicalChromeRevision();
		let installed=PupUtil.isChromeInstalled()?"Installed.":"Not installed.";

		return "Chromium r"+rev+": "+installed;		
	}

	static getCanonicalChromeRevision() {
		let pupIndex;

		try {
			pupIndex=require.resolve("puppeteer");
		}

		catch (e) {
			pupIndex=require.resolve("puppeteer-core");
		}

		let pupPath=path.dirname(pupIndex);
		let pupPackage=fs.readJsonSync(pupPath+"/package.json");
		let chromeRev=pupPackage.puppeteer.chromium_revision;

		if (!chromeRev)
			throw new Error("Puppeteer not found among deps(?).");

		return chromeRev;
	}

	static isChromeInstalled() {
		var browserFetcher=puppeteer.createBrowserFetcher();
		var revInfo=browserFetcher.revisionInfo(PupUtil.getCanonicalChromeRevision())

		return revInfo.local;
	}

	static ensureChromeInstalled() {
		let progressBar = null;
		let lastDownloadedBytes = 0;
		let revision=PupUtil.getCanonicalChromeRevision();

		function onProgress(downloadedBytes, totalBytes) {
			if (!progressBar) {
				const ProgressBar = require('progress');
				progressBar = new ProgressBar(`Downloading Chromium r${revision} - ${toMegabytes(totalBytes)} [:bar] :percent :etas `, {
					complete: '=',
					incomplete: ' ',
					width: 20,
					total: totalBytes,
				});
			}
			const delta = downloadedBytes - lastDownloadedBytes;
			lastDownloadedBytes = downloadedBytes;
			progressBar.tick(delta);
		}

		let browserFetcher=puppeteer.createBrowserFetcher();

		return new Promise((resolve,reject)=>{
			browserFetcher.download(revision,onProgress)
				.then(() => browserFetcher.localRevisions())
				.then(resolve)
				.catch(reject);
		});
	}
};

module.exports=PupUtil;