const Crawler=require("./Crawler");

class ForkCrawlRunnerWorker {
	constructor(path) {
		let crawler=Crawler.createFromPackage(path);
		this.runner=crawler.createCrawlRunner();
		this.runner.on("progress",(percent)=>{
			this.emit("progress",percent);
		});

		this.runner.on("log",(message)=>{
			this.emit("log",message);
		});

		this.runner.on("result",(row)=>{
			this.emit("result",row);
		});
	}

	setPupOptions(options) {
		this.runner.setPupOptions(options);
	}

	setAuth(auth) {
		this.runner.setAuth(auth)
	}

	setInputs(inputs) {
		this.runner.setInputs(inputs);
	}

	run() {
		return this.runner.run();
	}
}

module.exports=ForkCrawlRunnerWorker;