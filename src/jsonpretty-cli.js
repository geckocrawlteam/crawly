#!/usr/bin/env node

const fs=require("fs-extra");
let argv=require('minimist')(process.argv.slice(2));

if (!argv._.length) {
	console.log("Usage: jsonpressy <file [...]>");
	process.exit(1);
}

for (let fn of argv._) {
	try {
		let json=fs.readJsonSync(fn);
		fs.writeJsonSync(fn,json,{spaces: 2});
	}

	catch (e) {
		console.log("Problem with '"+fn+"': "+e.message);
	}
}