const EventEmitter=require("events");
const PupUtil=require("./PupUtil");
const puppeteer=require("./puppeteer.js");
const CsvUtil=require("./CsvUtil.js");

class CrawlRunner extends EventEmitter {

	constructor() {
		super();

		if (!PupUtil.isChromeInstalled())
			throw new Error("Chromuim not installed.");

		this.pupOptions={};
		this.data=[];
		this.log=this.log.bind(this);
		this.inputs={};
	}

	setInputs(inputs) {
		for (let inputSpec of this.crawler.getInputs()) {
			if (inputSpec.type=="list" && (typeof inputs[inputSpec.key])=="string")
				inputs[inputSpec.key]=CsvUtil.parseLine(inputs[inputSpec.key]);
		}

		this.inputs=inputs;
		this.input=inputs;
	}

	result(item) {
		this.data.push(item);
		this.emit("result",item);
	}

	getPage() {
		return this.page;
	}

	async injectJquery(page) {
		if (!page)
			page=this.page;

		await page.addScriptTag({path: require.resolve("jquery")});
	}

	setAuth(auth) {
		if (typeof(auth)=="string") {
			let userPass=auth.split(":");
			this.auth={
				username: userPass[0],
				password: userPass[1]
			};
		}

		else
			this.auth=auth;
	}

	setPupArg(arg) {
		if (!this.pupOptions.args)
			this.pupOptions.args=[];

		this.pupOptions.args.push(arg);
	}

	setPupOption(option, value) {
		this.pupOptions[option]=value;
	}

	setPupOptions(options) {
		this.pupOptions=options;
	}

	async renewPage() {
		let newPage=await this.browser.newPage();
		let pages=await this.browser.pages();
		for (let page of pages)
			if (page!=newPage)
				await page.close();

		this.page=newPage;

		if (this.auth)
			await this.page.authenticate(this.auth);

		await this.page.setViewport({width: 800, height: 500});

		return newPage;
	}

	async doRun() {
		if (!this.pupOptions.args)
			this.pupOptions.args=[];

		if (this.pupOptions.headless===false)
			this.pupOptions.args.push("--window-size=810,650");

		this.pupOptions.args.push("--disable-web-security");

		this.pupOptions.args.push("--no-sandbox");
		this.pupOptions.args.push("--deterministic-fetch");

		this.browser=await puppeteer.launch(this.pupOptions);

		var pages=await this.browser.pages();
		if (pages.length>=1)
			this.page=pages[0];

		else
			this.page=await this.browser.newPage();

		if (this.auth)
			await this.page.authenticate(this.auth);

		await this.page.setViewport({width: 800, height: 500});

		let p=this.crawler.crawlFunction(this);
		if (p instanceof Promise) {
			p.then((result)=>{
				this.done(result);
			});
			p.catch((err)=>{
				this.error(err);
			});
		}
	}

	run() {
		this.percentage=0;
		this.runPromise=new Promise((resolve,reject)=>{
			this.runPromiseResolve=resolve;
			this.runPromiseReject=reject;
			this.doRun()
				.catch((err)=>{
					this.runPromiseReject(err);
				});
		});

		return this.runPromise;
	}

	log(message) {
		this.emit("log",message);
	}

	error(message) {
		let p=Promise.resolve();
		if (this.browser) {
			p=this.browser.close();
			this.browser=null;
		}

		if (this.isComplete)
			return;

		this.isComplete=true;

		p.then(()=>{
			this.runPromiseReject(message);
		});
	}

	done(data) {
		let p=Promise.resolve();
		if (this.browser) {
			p=this.browser.close();
			this.browser=null;
		}

		if (this.isComplete)
			return;

		this.isComplete=true;

		p.then(()=>{
			this.percentage=100;

			if (Array.isArray(data))
				for (let item of data)
					this.result(item);

			this.runPromiseResolve(this.data);
		});
	}

	progress(percentage) {
		this.percentage=Math.round(percentage);
		this.emit("progress",this.percentage);
	}

	getProgress() {
		return this.percentage;
	}

	getData() {
		return this.data;
	}
}

module.exports=CrawlRunner;