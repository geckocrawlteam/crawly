let CrawlRunner=require("./CrawlRunner.js");
let ForkCrawlRunner=require("./ForkCrawlRunner.js");
let fs=require("fs");
let path=require("path");

class Crawler {
	constructor() {
		this.inputs=[];
	}

	static processInputSpec(specs) {
		let res=[];

		if (Array.isArray(specs)) {
			for (let item of specs)
				res.push(Crawler.processOneInputSpec(item));
		}

		else {
			for (let i in specs) {
				let item=specs[i];
				item.key=i;
				res.push(Crawler.processOneInputSpec(item));
			}
		}

		return res;
	}

	static processOneInputSpec(spec) {
		if (!spec.key)
			spec.key=spec.name;

		spec.name=spec.key;

		if (!spec.type)
			spec.type="text";

		if (["text","enum","list"].indexOf(spec.type)<0)
			throw new Error("Unknown input type: "+spec.type);

		if (spec.type=="enum" && !spec.enum)
			throw new Error("Type is enum, but no enums defined");

		if (Array.isArray(spec.enum)) {
			let tmp={};

			for (let item of spec.enum)
				tmp[item]=item;

			spec.enum=tmp;
		}

		return spec;
	}

	static createFromPackage(directory) {
		directory=path.resolve(directory);
		let stat;

		try {
			stat=fs.statSync(directory);
		}

		catch (e) {
			throw new Error(directory+": No crawler found here.");
		}

		if (!stat.isDirectory())
			throw new Error(directory+": Not a directory");

		try {
			stat=fs.statSync(directory+"/package.json");
		}

		catch (e) {
			throw new Error(directory+": No package.json found.");
		}

		let pkg=require(directory+"/package.json");
		let mod=require(directory);

		if (!pkg.crawler)
			throw new Error(directory+": No crawler defined in package.json.");

		if (!pkg.crawler.name)
			throw new Error(directory+": No crawler name defined in package.json.");

		if (!pkg.crawler.inputs)
			throw new Error(directory+": No crawler inputs defined in package.json.");

		let crawler=new Crawler();
		crawler.directory=directory;
		crawler.name=pkg.crawler.name;
		crawler.inputs=Crawler.processInputSpec(pkg.crawler.inputs);

		if (pkg.crawler.crawlFunction)
			crawler.crawlFunction=mod[pkg.crawler.crawlFunction];

		else
			crawler.crawlFunction=mod;

		if (typeof crawler.crawlFunction!="function")
			throw new Error("Crawl function not defined, or not a function.");

		return crawler;
	}

	static createFromCwd() {
		return Crawler.createFromPackage(process.cwd());
	}

	getName() {
		return this.name;
	}

	getInputs() {
		return this.inputs;
	}

	createCrawlRunner(mode) {
		let crawlRunner;

		switch (mode) {
			case "fork":
				crawlRunner=new ForkCrawlRunner();
				break;

			default:
				crawlRunner=new CrawlRunner();
				break;
		}

		crawlRunner.crawler=this;

		return crawlRunner;
	}

	getAbsolutePath() {
		return this.directory;
	}
}

module.exports=Crawler;