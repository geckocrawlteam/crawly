let readline = require('readline');

async function prompt(prompt) {
	return new Promise((resolve, reject)=>{
		let input;

		let rl=readline.createInterface(process.stdin, process.stdout);
		rl.setPrompt(prompt);
		rl.prompt();

		rl.on('line', (line)=>{
			input=line;
			rl.close();
		});

		rl.on('close',()=>{
			resolve(input);
		});
	});
}

/*(async ()=>{
	let s=await prompt("hello: ");
	console.log("s: "+s);
})();*/

module.exports=prompt;