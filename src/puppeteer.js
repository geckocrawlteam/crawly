let packageName;

try {
	require.resolve("puppeteer");
	packageName="puppeteer";
}

catch (e) {
	packageName="puppeteer-core";
}

module.exports=require(packageName);
