var Crawler=require("./Crawler");
var puppeteer=require("./puppeteer.js");
var PupUtil=require("./PupUtil");

module.exports={
	Crawler: Crawler,
	puppeteer: puppeteer,
	ensureChromeInstalled: PupUtil.ensureChromeInstalled,
	getChromeStatusString: PupUtil.getStatusString,
	isChromeInstalled: PupUtil.isChromeInstalled
}