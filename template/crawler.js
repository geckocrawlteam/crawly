module.exports=async function(crawl) {
	/*
		Here is where the crawler does its work.

		We can access parameters given by the user in the
		crawl.inputs object, e.g. crawl.inputs.input1

		When we are done with the crawl, we call
		crawl.done(result) with the result of the crawl.
	*/

	var result={
		value1: crawl.inputs.input1,
		value2: crawl.inputs.input2
	};

	crawl.done(result);
};