let crawly=require("../src/crawly.js");

describe("crawler",function() {
	it("can create a crawler",function() {
		let crawler=crawly.Crawler.createFromPackage(process.cwd()+"/spec/crawlers/test-crawler");
		let inputs=crawler.getInputs();

		expect(inputs.length).toEqual(2);
		expect(inputs[0].key).toEqual("name");
		expect(inputs[0].name).toEqual("name");
		expect(inputs[1].key).toEqual("age");
		expect(inputs[1].name).toEqual("age");
	});

	it("can create a crawler without a crawl function",function(done) {
		let crawler=crawly.Crawler.createFromPackage(process.cwd()+"/spec/crawlers/implicit-crawl-function-crawler");
		let runner=crawler.createCrawlRunner();
		runner.run().then(()=>{
			done();
		});
	});

	it("can list the crawler inputs",function() {
		let crawler=crawly.Crawler.createFromPackage(process.cwd()+"/spec/crawlers/input-crawler");
		let inputs=crawler.getInputs();

		expect(inputs.length).toEqual(3);
		expect(inputs[0].key).toEqual("arg1");
		expect(inputs[0].name).toEqual("arg1");
		expect(inputs[1].key).toEqual("arg2");
		expect(inputs[1].name).toEqual("arg2");

		expect(inputs[1].enum).toEqual({
			"hello":"hello",
			"world":"world"
		})

		expect(inputs[2].enum).toEqual({
			"opt1":"Hello",
			"opt2":"World"
		})
	});

});