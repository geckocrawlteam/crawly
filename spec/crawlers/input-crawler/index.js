function sleep(millis) {
	return new Promise((resolve, reject)=>{
		setTimeout(resolve,millis);
	});
}

module.exports=async function(crawl) {
	await sleep(500);
	//crawl.done([{"x":3}]);
	return [
		{"x":1},
		{"x":2},
	];
}