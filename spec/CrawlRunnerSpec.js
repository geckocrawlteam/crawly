let Crawler=require("../src/Crawler.js");
let Crawl=require("../src/CrawlRunner.js");

describe("CrawlRunner", function(){
	it("can run a crawl", function(done){
		let crawler=Crawler.createFromPackage(process.cwd()+"/spec/crawlers/test-crawler");
		let testCrawl=crawler.createCrawlRunner();
		let inputs = {"name": "Test"};

		testCrawl.setInputs(inputs);
		testCrawl.run().then(()=>{
			let data=testCrawl.getData();
			expect(data).toEqual(["hello Test"]);
			done();
		});
	});

	it("can run a crawl that returns a promise", async function() {
		let crawler=Crawler.createFromPackage(process.cwd()+"/spec/crawlers/promise-crawler");
		let crawlRunner=crawler.createCrawlRunner();

		let res=await crawlRunner.run();
		expect(res[0].x).toEqual(1);
	});

	it("can run a crawl that delivers partial results", async function() {
		let crawler=Crawler.createFromPackage(process.cwd()+"/spec/crawlers/result-crawler");
		let crawlRunner=crawler.createCrawlRunner();

		let invoked=0;
		crawlRunner.on("result",(row)=>{
			invoked++;
		});

		let res=await crawlRunner.run();
		expect(res.length).toEqual(4);
		expect(invoked).toEqual(4);
	});

	it("can run a fork crawl that delivers partial results", async function() {
		let crawler=Crawler.createFromPackage(process.cwd()+"/spec/crawlers/result-crawler");
		let crawlRunner=crawler.createCrawlRunner("fork");

		let invoked=0;
		crawlRunner.on("result",(row)=>{
			invoked++;
		});

		let res=await crawlRunner.run();
		expect(res.length).toEqual(4);
		expect(invoked).toEqual(4);
	});

	it("can run a crawl as a separate process", async function(done) {
		let crawler=Crawler.createFromPackage(process.cwd()+"/spec/crawlers/exit-crawler");
		let crawlRunner=crawler.createCrawlRunner("fork");

		crawlRunner.run().catch((e)=>{
			expect(e).toEqual("Unexpected process termination");
			done();
		});
	});

	it("arrays can be used for lists", async function() {
		let crawler=Crawler.createFromPackage(process.cwd()+"/spec/crawlers/list-crawler");
		let crawlRunner=crawler.createCrawlRunner();
		crawlRunner.setInputs({
			values: ["hello","world"]
		});

		let res=await crawlRunner.run();
		expect(res).toEqual([{value: "hello"},{value: "world"}]);
	});

	it("csv strings can be used for lists", async function() {
		let crawler=Crawler.createFromPackage(process.cwd()+"/spec/crawlers/list-crawler");
		let crawlRunner=crawler.createCrawlRunner();
		crawlRunner.setInputs({
			values: "hello,world"
		});

		let res=await crawlRunner.run();
		expect(res).toEqual([{value: "hello"},{value: "world"}]);
	});
});